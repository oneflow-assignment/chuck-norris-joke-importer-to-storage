#!/usr/bin/env python3

import requests
import mysql.connector

# Connect to the MySQL server
mydb = mysql.connector.connect(
    host="storage-mysql.storage-mysql.svc.cluster.local",
    user="chuck_norris_joke",
    password="Dtamtus_007",
    database="chuck_norris_joke",
    auth_plugin='mysql_native_password'
)
mycursor = mydb.cursor()

# Create the jokes table if it doesn't exist
mycursor.execute("CREATE TABLE IF NOT EXISTS jokes (id INT AUTO_INCREMENT PRIMARY KEY, joke VARCHAR(255))")

# Call the Chuck Norris API to get a random joke
url = "https://api.chucknorris.io/jokes/random"
response = requests.get(url)
joke = response.json()['value']

# Insert the joke into the jokes table
sql = "INSERT INTO jokes (joke) VALUES (%s)"
val = (joke,)
mycursor.execute(sql, val)

# Commit the changes to the database
mydb.commit()

# Close the cursor and the database connection
mycursor.close()
mydb.close()
