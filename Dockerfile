FROM python:slim-bullseye
RUN python3 -m pip install mysql-connector-python requests
COPY joke_importer.py /usr/local/bin/
RUN chmod +x /usr/local/bin/joke_importer.py
ENTRYPOINT ["tail", "-f", "/dev/null"]
